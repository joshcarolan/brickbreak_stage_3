#ifndef DIALOG_H
#define DIALOG_H

#include <QTimer>
#include <QDialog>
#include <QGraphicsView>
#include <QGraphicsScene>

#include<QKeyEvent>
#include "itemfactory.h"
#include <list>
#include <QSplashScreen>
#include <QPixmap>


namespace Ui{
class Dialog;
}

class Dialog : public QDialog{
    Q_OBJECT

public:
    explicit Dialog(Config::Config *con, QWidget *parent = 0);
    ~Dialog();

    QGraphicsScene * getScene(){ return scene; }
    void keyPressEvent(QKeyEvent *keypress);
    void notifyObservers(QString keypressed);
    void scoreUpdate();
    int getHighScore();
public slots:
    void updateScore(int value);
    void updateLives(int value);
private:
    Ui::Dialog *ui;
    QGraphicsView *view;
    QGraphicsScene *scene;
    Config::Config *config;

    Paddle * paddle;
    Ball *ball;
    QSplashScreen *gameOver = new QSplashScreen;
    QPixmap gameOverPixMap;


};

#endif // DIALOG_H
