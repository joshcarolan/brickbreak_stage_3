#ifndef BRICKCOMPONENT_H
#define BRICKCOMPONENT_H

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <iostream>
#include "ball.h"

/* A BrickComponent is a abstract class providing an interface for
 * brick items which can be added to a QGraphicsScene. A brick
 * subclass and various brick decorators provide the concrete implementation.
 * A brick can have any number of decorators.
*/
class Ball;
class BrickComponent : public QGraphicsItem
{

public:
    virtual ~BrickComponent(){}

    /* The QGraphicsScene searches for the QGraphicsItem::advance and paint
     * in order to animate the brick
     */
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) = 0;
    virtual void advance(int phase) = 0;

    virtual QRectF boundingRect() const = 0;

    /* Called by another item when it collides with a brick.
     * Brick responds to the collision in various ways depending on
     * its type
     */
    virtual void collision(QGraphicsScene *scene, Ball *ball) = 0;

    // Helper functions
    virtual int getLives() = 0;
    virtual void setLives(int i) = 0;
    virtual QPointF getPosition() = 0;
    virtual void setPosition(int x, int y) = 0;
    virtual std::string getColor() = 0;
    virtual void setColor(std::string newColor) = 0;

};

#endif // BRICKCOMPONENT_H
