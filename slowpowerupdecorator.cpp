#include "slowpowerupdecorator.h"
/**
 * @brief MultiColorDecorator::collision
 * Reimplements the collision function to slow the ball down when the brick is destroyed
 * @param scene
 * the scene that the brick is a part of
 * @param ball
 * the ball that collided with the brick
 */
void SlowPowerupDecorator::collision(QGraphicsScene *scene, Ball * ball)
{
    // If scene contains this object and lives < 1, remove from scene ("outermost wrapper")
    if (this->scene() != 0)
    {
        brick->setLives(-1);
        if (brick->getLives() == 0)
        {
            ball->mulitplyyVelocity(0.5);
            ball->multiplyxVelocity(0.5);
            this->scene()->removeItem(this);
            return;
        }
    }
    brick->collision(scene, ball);

}
