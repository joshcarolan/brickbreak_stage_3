#ifndef TELEPORTDECORATOR_H
#define TELEPORTDECORATOR_H

#include "decorator.h"

/* Concrete Decorator for a Brick.
 * Reimplements the collision function so that the position of the brick changes.
 */
class TeleportDecorator : public Decorator
{

public:
    TeleportDecorator(BrickComponent * brick):Decorator(brick){setPos(brick->pos());}
    virtual ~TeleportDecorator(){}

    void collision(QGraphicsScene *scene, Ball *ball=nullptr);

};

#endif // TELEPORTDECORATOR_H
