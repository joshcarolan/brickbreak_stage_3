#ifndef MULTICOLORDECORATOR_H
#define MULTICOLORDECORATOR_H

#include "decorator.h"

/* Concrete Decorator for a Brick.
 * Reimplements the collision function so that the color of the brick changes.
 */
class MultiColorDecorator : public Decorator
{

public:
    MultiColorDecorator(BrickComponent * brick) : Decorator(brick) {setPos(brick->pos());}
    virtual ~MultiColorDecorator(){}

    void collision(QGraphicsScene *scene, Ball *ball=nullptr);

};

#endif // MULTICOLORDECORATOR_H
