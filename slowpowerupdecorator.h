#ifndef SLOWPOWERUPDECORATOR_H
#define SLOWPOWERUPDECORATOR_H
#include "decorator.h"
#include "ball.h"
/* Concrete Decorator for a Brick.
 * Reimplements the collision function so that the speed of the ball is decreased when the brick is destroyed.
 */

class SlowPowerupDecorator: public Decorator
{
public:
    SlowPowerupDecorator(BrickComponent * brick): Decorator(brick) {setPos(brick->pos());}
    virtual ~SlowPowerupDecorator(){}
    void collision(QGraphicsScene *scene, Ball *ball);
};

#endif // SLOWPOWERUPDECORATOR_H
