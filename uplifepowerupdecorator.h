#ifndef UPLIFEPOWERUPDECORATOR_H
#define UPLIFEPOWERUPDECORATOR_H
#include "decorator.h"
#include "ball.h"
/* Concrete Decorator for a Brick.
 * Reimplements the collision function so that the ball gains a life when the brick is destroyed.
 */
class UpLifePowerupDecorator:public Decorator
{
public:
    UpLifePowerupDecorator(BrickComponent * brick): Decorator(brick) {setPos(brick->pos());}
    virtual ~UpLifePowerupDecorator(){}
    void collision(QGraphicsScene *scene, Ball *ball);
};

#endif // UPLIFEPOWERUPDECORATOR_H
