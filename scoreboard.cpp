#include "scoreboard.h"

ScoreBoard::ScoreBoard():lives(0)
{
}

ScoreBoard::ScoreBoard(ScoreBoard *sb)
    :lives(sb->getLives())
    ,score(sb->getScore())
    ,height(sb->getHeight())
{
    scoreLabel.setText(QString::fromStdString("Score: "));
    livesLabel.setText(QString::fromStdString("Lives: "));
    frame.setFixedHeight(sb->getHeight());
    frame.setFixedWidth(sb->getWidth());
}

ScoreBoard::ScoreBoard(int ballLives, int h, int w)
    :lives(ballLives)
    ,height(h)
    ,width(w)
    ,score(0)
{
    scoreLabel.setText(QString::fromStdString("Score: "));
    livesLabel.setText(QString::fromStdString("Lives: "));
    frame.setFixedHeight(h);
    frame.setFixedWidth(w);
}

void ScoreBoard::incrementLives(int incrementBy)
{
    lives+=incrementBy;
    livesDisp.display(lives);
}

void ScoreBoard::decrementLives(int decrementBy)
{
    lives-=decrementBy;
    livesDisp.display(lives);
}

void ScoreBoard::incrementScore(int incrementBy)
{
    score+=incrementBy;
    scoreDisp.display(score);
}
void ScoreBoard::decrementScore(int decrementBy)
{
    score-=decrementBy;
    scoreDisp.display(score);
}
int ScoreBoard::getLives()
{
    return lives;
}
int ScoreBoard::getScore(){
    return score;
}
int ScoreBoard::getHeight(){
    return height;
}
int ScoreBoard::getWidth(){
    return width;
}

QRectF ScoreBoard::boundingRect()
{
    return QRectF(0, 0, width, height);
}

