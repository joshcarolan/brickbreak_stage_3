 #ifndef BRICK_H
#define BRICK_H

#include <sstream>
#include <QPainter>
#include "brickcomponent.h"
#include "config.h"

/* Concrete implementation of BrickComponent.*/
class Brick : public BrickComponent
{

public:
    Brick(std::map<std::string, double> brick,  Config::Config *config);
    virtual ~Brick(){}

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    void advance(int phase) {if(phase==0){moveBy(1,1);moveBy(-1,-1);}}

    void collision(QGraphicsScene *scene, Ball *ball=nullptr);

    // Helper functions
    int getLives() {return lives;}
    void setLives(int i) {lives += i;}
    QPointF getPosition(){return pos();}
    void setPosition(int x, int y) {setPos(x, y);}
    std::string getColor(){return color;}
    void setColor(std::string newColor) {color = newColor;}

private:
    std::string colorToString(int color);

    int width;
    int height;
    int lives;
    std::string color;

};

#endif // BRICK_H
