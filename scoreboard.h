#ifndef SCOREBOARD_H
#define SCOREBOARD_H
#include <QFrame>
#include <QLCDNumber>
#include <QLabel>
#include <QGraphicsScene>
#include <QGroupBox>
class ScoreBoard{
public:
    ScoreBoard();
    ScoreBoard(ScoreBoard *sb);
    ScoreBoard(int ballLives, int h, int w);
    void incrementLives(int incrementBy);
    void decrementLives(int decrementBy);
    void incrementScore(int incrementBy);
    void decrementScore(int decrementBy);
    int getLives();
    int getScore();
    int getHeight();
    int getWidth();
    QRectF boundingRect();

    //void advance(int phase);
    //void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
private:
    int score;
    QLCDNumber scoreDisp;
    int lives;
    QLCDNumber livesDisp;
    QLabel scoreLabel;
    QLabel livesLabel;
    int height;
    int width;
    QFrame frame;
    QGroupBox box;
};

#endif // SCOREBOARD_H
