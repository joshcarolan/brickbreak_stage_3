#ifndef CONFIG_H
#define CONFIG_H

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <map>
#include<QColor>

namespace Config{
    //change this path depending on the location of your config file

    const std::string CONFIG_PATH ="../AssignmentThreeBaseOne/config.config";

    //const std::string CONFIG_PATH ="config.config";


    class Config{
    public:
        Config() : xCoordinate(0), yCoordinate(0), radius(10), xVelocity(1), yVelocity(1), height(400), width(400), bricks(),paddleHeight(0),paddleWidth(0),paddleStartX(0), paddleVelocity(0), ballLives(0){
            readFile();
        }

        void readFile();
        void validate();
        std::string removeSpace(std::string);

        int getxCoordinate() { return xCoordinate; }
        int getyCoordinate() { return yCoordinate; }
        int getRadius() { return radius; }
        float getxVelocity() { return xVelocity; }
        float getyVelocity() { return yVelocity; }
        int getHeight() { return height; }
        int getWidth() { return width; }
        std::vector<std::map<std::string, double>> getBricks() { return bricks; }
        int getPaddleHeight() { return paddleHeight;}
        int getPaddleWidth() { return paddleWidth;}
        int getPaddleStartX(){ return paddleStartX;}

        int getPaddleColour() { return paddleColour;}
        int getPaddleVelocity() { return paddleVelocity;}
        int getBallLives(){return ballLives;}
        std::string getPaddleLeftCommand(){return leftCommand;}
        std::string getPaddleRightCommand(){return rightCommand;}

    private:
        std::string getValue(std::string str) const;
        void addBrick(std::string str);

        int xCoordinate;
        int yCoordinate;
        int radius;
        float xVelocity;
        float yVelocity;
        int height;
        int width;
        std::vector<std::map<std::string, double>> bricks;
        int paddleHeight;
        int paddleWidth;
        int paddleStartX;

        int paddleColour;
        int paddleVelocity;
        int ballLives;

        std::string leftCommand;
        std::string rightCommand;
    };
}
#endif // CONFIG_H
