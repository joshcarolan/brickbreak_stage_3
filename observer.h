#ifndef OBSERVER_H
#define OBSERVER_H
#include <QString>
class Observer{
public:
    virtual void notify(QString keypress)=0;
};

#endif // OBSERVER_H
