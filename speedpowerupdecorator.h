#ifndef SPEEDPOWERUPDECORATOR_H
#define SPEEDPOWERUPDECORATOR_H
#include "decorator.h"
#include "ball.h"
/* Concrete Decorator for a Brick.
 * Reimplements the collision function so that the speed of the ball is increased when the brick is destroyed.
 */
class SpeedPowerupDecorator: public Decorator
{
public:
    SpeedPowerupDecorator(BrickComponent * brick): Decorator(brick) {setPos(brick->pos());}
    virtual ~SpeedPowerupDecorator(){}
    void collision(QGraphicsScene *scene, Ball *ball=nullptr);

};

#endif // SPEEDPOWERUPDECORATOR_H
