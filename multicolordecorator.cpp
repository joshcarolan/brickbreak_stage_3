#include "multicolordecorator.h"

/**
 * @brief MultiColorDecorator::collision
 * Reimplements the collision function to change brick's color
 * @param scene
 * the scene that the brick is a part of
 * @param ball
 * the ball that collided with the brick
 */
void MultiColorDecorator::collision(QGraphicsScene *scene, Ball *ball)
{
    // If scene contains this object and lives < 1, remove from scene ("outermost wrapper")
    if (this->scene() != 0)
    {
        brick->setLives(-1);
        if (brick->getLives() == 0)
        {
            this->scene()->removeItem(this);
            return;
        }
    }

    std::vector<std::string>colors = {"119136153", "176196222", "230230250"};
    int i = 0;

    // Cycles through colors on collision
    for (auto it = colors.begin(); it != colors.end(); it++){
        if (brick->getColor() == colors.at(i))
        {
            i = ++i % 3;
            break;
        }
        i++;
    }

    brick->setColor(colors.at(i > 2 ? 0 : i));
    brick->collision(scene, ball);
}
