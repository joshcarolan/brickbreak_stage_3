#include "speedpowerupdecorator.h"
/**
 * @brief MultiColorDecorator::collision
 * Reimplements the collision function to speed the ball up when the brick is destroyed
 * @param scene
 * the scene that the brick is a part of
 * @param ball
 * the ball that collided with the brick
 */
void SpeedPowerupDecorator::collision(QGraphicsScene *scene, Ball * ball)
{
    // If scene contains this object and lives < 1, remove from scene ("outermost wrapper")
    if (this->scene() != 0)
    {
        brick->setLives(-1);
        if (brick->getLives() == 0)
        {
            ball->mulitplyyVelocity(2);
            ball->multiplyxVelocity(2);
            this->scene()->removeItem(this);
            return;
        }
    }
    brick->collision(scene, ball);

}
