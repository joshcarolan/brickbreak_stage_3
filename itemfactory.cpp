#include "itemfactory.h"
/**
 * @brief ItemFactory::make
 * creates a new QGraphicsItem depending on the item string passed to the method
 * @param item
 * string that identifies the item to be produced by the factory
 * @param config
 * a pointer to the configuration instance that holds all of the configuration data
 * @return
 */
QGraphicsItem* ItemFactory::make(std::string item, Config::Config *config){

    if(item == "ball"){
        Ball *ball = new Ball(config->getxCoordinate(), config->getyCoordinate(), config->getRadius(),
                          config->getxVelocity(), config->getyVelocity(), "#84D9CF", config);
        return ball;
    }
    if(item == "paddle"){

        Paddle *paddle = new Paddle(config->getPaddleVelocity(), config->getPaddleStartX(),config->getHeight() -config->getPaddleHeight(), config->getPaddleWidth(),config->getPaddleHeight(),config->getWidth(), config->getPaddleColour(), config->getPaddleLeftCommand(),config->getPaddleRightCommand());
        return paddle;
    }

    return NULL;
}


/**
 * @brief ItemFactory::make
 * creates a brick component to add to the scene and adds various
 * decorators to the bricks based on the configuration
 * @param item
 * string that identifies the item to be produced in the factory
 * @param param
 * string double pair map of configuration information for the brick to be created
 * @param config
 * a pointer to the configuration instance that holds all of the configuration data
 * @return
 */
QGraphicsItem *ItemFactory::make(std::string item, std::map<std::string, double> param,  Config::Config *config)
{
    if(item == "brick"){
        BrickComponent *brick = new Brick(param, config);
        if (param["multicolorFlag"] == 1)
        {
            brick = new MultiColorDecorator(brick);
        }
        if (param["teleportFlag"] == 1)
        {
            brick = new TeleportDecorator(brick);
        }
        if(param["speedFlag"] == 1)
        {
            brick = new SpeedPowerupDecorator(brick);
        }
        if(param["slowFlag"] == 1)
        {
            brick = new SlowPowerupDecorator(brick);
        }
        if(param["upLifeFlag"] == 1)
        {
            brick = new UpLifePowerupDecorator(brick);
        }
        return brick;
    }
    return NULL;

}

