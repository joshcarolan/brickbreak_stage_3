#include "dialog.h"
#include "ui_dialog.h"
#include <QtWidgets>
/**
 * @brief Dialog::Dialog
 * Creates a new instance of dialog which in turn creates the scene and all elements in the scene:
 * ie ball, bricks and paddle as well as setting up the ui and ensuring that bricks arent placed on top of each other
 * @param con
 * pointer to a Config object which has all of the configuration items stored in it
 * @param parent
 * the QWidget parent of the dialog class
 */
Dialog::Dialog(Config::Config *con, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
    , config(con)
    ,  gameOverPixMap("./../AssignmentThreeBaseOne/game-over.jpg")
{
    view = new QGraphicsView(this);
    scene = new QGraphicsScene(this);
    scene->setSceneRect(0, 0, config->getWidth(), config->getHeight());
    //create ball and add to the scene
    ball = dynamic_cast<Ball*>(ItemFactory::make("ball", config));
    scene->addItem(ball);
    //create paddle and add to the scene
    paddle = dynamic_cast<Paddle*>(ItemFactory::make("paddle", config));
    scene->addItem(paddle);
    if(scene->collidingItems(paddle).size() > 0)
    {
        std::cout<<"Error: cannot place the paddle and the ball on top of each other"<<std::endl;
        exit(0);
    }

    // Add each brick to scene if it does not collide with other objects in scene
    for (size_t i = 0; i != config->getBricks().size(); i++)
    {
        BrickComponent *brick = dynamic_cast<BrickComponent*>(ItemFactory::make("brick", config->getBricks().at(i),config));

        if (scene->collidingItems(brick).size() == 0)
        {
            scene->addItem(brick);
        }
    }
    gameOverPixMap.scaledToHeight(config->getHeight());
    gameOverPixMap.scaledToWidth(config->getWidth());
    gameOver->setPixmap(gameOverPixMap);
    //gameOver->setFixedSize(config->getWidth()+4, config->getHeight()+4);
    ui->setupUi(this);
    ui->graphicsView->setScene(scene);
    ui->statusGroupBox->setFixedWidth(config->getWidth()+100);
    ui->LivesLCD->display(ball->lives);
    ui->scoreLCD->display(ball->score);
    ui->highScoreLCD->display(getHighScore());
    ui->graphicsView->setFixedHeight(config->getHeight()+4);
    ui->graphicsView->setFixedWidth(config->getWidth()+4);


    QObject::connect(ball,SIGNAL(scoreChanged(int)), this, SLOT(updateScore(int)));
    QObject::connect(ball,SIGNAL(livesChanged(int)), this, SLOT(updateLives(int)));
}
/**
 * @brief Dialog::~Dialog
 * Dialog destructor: when deleting scene, scene also deletes all other items associated with it
 */

Dialog::~Dialog()
{
    delete ui;
    delete view;
    delete scene;
}
/**
 * @brief Dialog::keyPressEvent
 * detects key presses at runtime and calls the nofityObservers method with the text of the key that was pressed
 * @param keypress
 * key that was pressed
 */
void Dialog::keyPressEvent(QKeyEvent *keypress)
{
    notifyObservers(keypress->text());

}
/**
 * @brief Dialog::notifyObservers
 * notifies the observers of the keyboard what the text of the key was that was pressed.
 * The only observer of the keyboard in this case is the paddle for movign the paddle left and right
 * @param keypress
 * the text of the key that was pressed
 */
void Dialog::notifyObservers(QString keypress)
{
    paddle->notify(keypress);
}

/**
 * @brief Dialog::updateScore
 * Slot that picks up the scoreChanged(int) signal from ball.
 * @param value
 * the value to be displayed by the QLCDNumber
 */
void Dialog::updateScore(int value){
    ui->scoreLCD->display(value);
}
/**
 * @brief Dialog::updateLives
 * Slot that picks up the livesChanged(int) signal from ball. When the number of lives has been
 * updated to that the dialog can display the correct number of lives reamining on the screen.
 * If the game is over, ie the number of lives for the ball reaches zero, it displays a 'game over'
 * splash screen.
 * @param value
 * the value to be displayed by the QLCDNumber
 */
void Dialog::updateLives(int value){
    ui->LivesLCD->display(value);
    if(value <= 0)
    {
        gameOver->show();
    }
}
/**
 * @brief Dialog::getHighScore
 * reads in the current saved high score from a text file
 * @return
 * the current high score
 */
int Dialog::getHighScore(){
    std::string filename= "./../AssignmentThreeBaseOne/highScores.txt";
    std::ifstream file(filename.c_str());
    int highscoreInt;
    if(file.fail()){
        std::cout<<"Error: Class: Ball, Line: 224 - Missing highscore file"<<std::endl;
        exit(0);
    }
    if (file.is_open()){
        std::string highscoreString;
        std::getline(file, highscoreString);
        std::cout<<highscoreString<<std::endl;
        highscoreInt = atoi(highscoreString.c_str());
    }
    file.close();
    return highscoreInt;
}
