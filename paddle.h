#ifndef PADDLE_H
#define PADDLE_H
#include <QColor>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QKeyEvent>
#include "config.h"
#include "observer.h"
class Paddle : public QGraphicsItem, public Observer{

public:
    /*Paddle(){}*/
    Paddle(int vel,int x, int y, int w,int h, int wWidth, int col, std::string leftKey, std::string rightKey)
        :velocity(vel)
        ,xPos(x)
        ,yPos(y)
        ,width(w)
        ,height(h)
        ,windowWidth(wWidth)
        ,color("")
        ,keyLeft(leftKey)
        ,keyRight(rightKey)
    {
         color = colourToString(col);
        //setFlag(QGraphicsItem::ItemIsFocusable,true);
        //std::cout<<height<<std::endl;
    }
/*
    Paddle(Paddle*&)
        :xPos(this->getXPos())
        ,yPos(this->getYPos())
        ,width(this->getWidth())
        ,height(this->getHeight())
        ,windowWidth(this->getWindowWidth())
        ,colour(this->getColour())
    {}
*/
/*
    Paddle (const Paddle&)
        :xPos(this->getXPos())
        ,yPos(this->getYPos())
        ,width(this->getWidth())
        ,height(this->getHeight())
        ,windowWidth(this->getWindowWidth())
        ,colour(this->getColour())
    {}
*/
    void notify(QString keypress);
    void moveLeft();
    void moveRight();
    void advance(int phase);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
    QRectF boundingRect() const;
    int getXPos(){return xPos;}
    int getYPos(){return yPos;}
    int getWidth(){return width;}
    int getHeight(){return height;}
    int getWindowWidth(){return windowWidth;}
    //QColor getColour(){return colour;}
    std::string colourToString(int colour);


private:
    int velocity;
    int xPos;
    int yPos;
    int width;
    int height;
    int windowWidth;
    std::string color;
    std::string keyLeft;
    std::string keyRight;
};

#endif // PADDLE_H
