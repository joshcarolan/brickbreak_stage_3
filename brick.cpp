#include "brick.h"
/**
 * @brief Brick::Brick
 *      Constructor for a brick, takes in a map of stings and
 *      doubles to initialise the bricks member vairables such
 *      as width, height, colour and position
 * @param brick
 *      A map of the brick settings to be given to the brick
 */
Brick::Brick(std::map<std::string, double> brick, Config::Config *config)
    : width(brick["width"])
    , height(brick["height"])
    , lives(brick["lives"])
    , color("")
{
    color = colorToString(brick["color"]);
    setPos(brick["xCoordinate"], brick["yCoordinate"]);
    if(brick["yCoordinate"]+ brick["height"]> config->getHeight() - config->getPaddleHeight())
    {
        std::cout<<"Cannot place bricks in the path of the paddles movement"<<std::endl;
        exit(0);
    }
}

/* Converts color provided as an int to RGB string */
/**
 * @brief Brick::colorToString
 *      Converts color provided as an int to RGB string
 * @param color
 *      color as an integer
 * @return
 *      color as a string
 */
std::string Brick::colorToString(int color)
{
    std::stringstream s, r;
    for (int i = 0; i != 3; i++, color/= 1000){
        int c = color % 1000;
        if (c < 10)
        {
            s << "00" << c;
        }
        else if (c < 100)
        {
            s << "0" << c;
        } else
        {
            s << c;
        }

    }
    r << s.str().substr(6,3) << s.str().substr(3,3) << s.str().substr(0,3);
    return r.str();

}

/**
 * @brief Brick::paint
 *      Paints the brick as a rectangle based on height, width and color
 * @param painter
 *      Qpainter object for drawing the item
 * @param option
 *      QStyleOptionGraphicsItem options for drawing the item
 * @param widget
 *      QWidget the parent widget
 */
void Brick::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(QPen(Qt::black));
    painter->setBrush(QBrush(QColor(atoi(color.substr(0,3).c_str()),atoi(color.substr(3,3).c_str()),atoi(color.substr(6,3).c_str()))));
    painter->drawRect(boundingRect());
}
/**
 * @brief Brick::boundingRect
 *      the boundingRect of an object is the rectangle that encapsulates the whole object
 * @return
 *      the bouncind rect of the brick
 */
QRectF Brick::boundingRect() const
{
    return QRectF(0, 0, width, height);
}


/**
 * @brief Brick::collision
 *      Called by an item if it collides with a brick.
 *      The brick decreases its lives by 1, removing itself from the scene if lives < 1
 * @param scene
 *      the scene the brick is a part of
 * @param ball
 * the ball that collided with the brick
 */
void Brick::collision(QGraphicsScene *scene, Ball* ball)
{
    // If scene contains this object and lives < 1, remove from scene ("outermost wrapper")
    if (this->scene() != 0)
    {
        lives -= 1;
        if (lives <= 0 )
        {
            this->scene()->removeItem(this);
        }
    }
}
