#ifndef DECORATOR_H
#define DECORATOR_H

#include "brickcomponent.h"

/* Abstract decorator for a Brick.
 * A Decorator has a BrickComponent* member variable which it uses
 * to implement the BrickComponent interface
 */
class Decorator : public BrickComponent
{
public:
    Decorator(BrickComponent *brick): brick(brick){setPos(brick->pos());}
    virtual ~Decorator(){delete brick;}

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){brick->paint(painter, option, widget);}
    virtual QRectF boundingRect() const {return brick->boundingRect();}
    virtual void advance(int phase) {if(phase==0){moveBy(1,1);moveBy(-1,-1);setPos(brick->getPosition());}}

    virtual void collision(QGraphicsScene *scene, Ball *ball=nullptr){brick->collision(scene, ball);}

    virtual int getLives() {return brick->getLives();}
    virtual void setLives(int i) {brick->setLives(i);}
    virtual QPointF getPosition() {return brick->getPosition();}
    virtual void setPosition(int x, int y) {setPos(x, y); brick->setPosition(x, y);}
    virtual std::string getColor() {return brick->getColor();}
    virtual void setColor(std::string newColor) {brick->setColor(newColor);}

protected:
    BrickComponent* brick;

};

#endif // DECORATOR_H
