#include "teleportdecorator.h"
/**
 * @brief TeleportDecorator::collision
 * Reimplements the collision function to casue the brick to teleport
 * @param scene
 * the scene that the brick is a part of
 */
void TeleportDecorator::collision(QGraphicsScene *scene, Ball *ball)
{
    // If scene contains this object and lives < 1, remove from scene ("outermost wrapper")
    if (this->scene() != 0)
    {
        brick->setLives(-1);
        if (brick->getLives() == 0)
        {
            this->scene()->removeItem(this);
            return;
        }
    }

    // Reset to random position, checking for collisions
    int width = scene->width(), height = scene->height();
    int x = qrand() % width, y =  qrand() % height;
    setPosition(x, y);
    while(scene->collidingItems(this).size() != 0 ||
          x > width - boundingRect().width() ||
          y > height - boundingRect().height())
    {
        x = qrand() % width;
        y = qrand() % height;
        setPosition(x, y);
    }

    brick->collision(scene, ball);
}
