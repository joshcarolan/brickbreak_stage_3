#ifndef BALL_H
#define BALL_H

#include <iostream>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPoint>
#include <QPainter>
#include <QColor>
#include "config.h"
#include "brickcomponent.h"
#include "paddle.h"
#include "scoreboard.h"
#include <QLCDNumber>

class Ball : public QObject,  public QGraphicsItem{
    Q_OBJECT
public:
    Ball(int xCoordinate, int yCoordinate, int radius, float xVelocity, float yVelocity, QColor colour,
         Config::Config * config)
        : score(0)
        , lives(config->getBallLives())
        , position(new QPoint(xCoordinate, yCoordinate))
        , radius(radius)
        , xVelocity(xVelocity)
        , yVelocity(yVelocity)
        , boxWidth(config->getWidth())
        , boxHeight(config->getHeight())
        , colour(colour)
        , paddleHeight(config->getPaddleHeight())
        , startX(xCoordinate)
        , startY(yCoordinate)
        //, sb(new ScoreBoard(config->getBallLives(),config->getScoreHeight(),config->getWidth()))
    {
         setPos(mapToScene(*position));
         //if all the default values for a paddle are active then there is no paddle in config file
         if(config->getPaddleWidth() ==0 && config->getPaddleHeight()==0 && config->getPaddleVelocity() ==0&& config->getPaddleStartX()==0 && config->getBallLives()==0)
         {
             thereIsPaddle = false;
         }
         else
         {
            thereIsPaddle = true;
         }
         //this->scene()->addItem(sb);
         //scoreDisp.setParent(this->pa);
         //scoreDisp.display(score);
    }

    virtual ~Ball(){
        delete position;
        //delete sb;
    }

    /* The QGraphicsScene searches for the QGraphicsItem::advance and paint
     * in order for us to animate the ball
     */
    void advance(int phase);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);

    /* Frames around the item (ball). This function returns QRectF(0, 0, radius*2, radius*2)
     * meaning the origin of the ball/item has moved from the center to the top
     * left corner of the frame
     */
    QRectF boundingRect() const { return QRectF(0, 0, radius*2, radius*2); }
    QPainterPath shape() const;

    bool brickCollision(int &futureXPos, int &futureYPos);
    bool paddleCollision(int &futureXPos, int &futureYPos);
    QPoint * getPosition() { return position; }
    int getRadius() { return radius; }
    float getxVelocity() { return xVelocity; }
    float getyVelocity() { return yVelocity; }
    int getBoxWidth() { return boxWidth; }
    int getBoxHeight() { return boxHeight; }
    void checkScore();
    void multiplyxVelocity(float multiplier){xVelocity *= multiplier;}
    void mulitplyyVelocity(float multiplier){yVelocity *=multiplier;}
    void incrementLives(int incrementBy);
    int score;
    int lives;
signals:
    void scoreChanged(int newValue);
    void livesChanged(int newValue);
private:
    QPoint *position;
    int radius;
    float xVelocity;
    float yVelocity;
    int boxWidth;
    int boxHeight;
    QColor colour;
    int paddleHeight;
    int startX;
    int startY;
    bool thereIsPaddle;
    //ScoreBoard *sb;
    //QLCDNumber scoreDisp;
};

#endif // BALL_H
