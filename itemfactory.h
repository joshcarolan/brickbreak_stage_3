#ifndef ITEMFACTORY_H
#define ITEMFACTORY_H

#include <iostream>
#include "ball.h"
#include "config.h"
#include "brick.h"
#include "teleportdecorator.h"
#include "multicolordecorator.h"
#include "paddle.h"
#include "speedpowerupdecorator.h"
#include "slowpowerupdecorator.h"
#include "uplifepowerupdecorator.h"
class ItemFactory{
public:
    static QGraphicsItem* make(std::string item, Config::Config *config);
    static QGraphicsItem* make(std::string item, std::map<std::string, double> param, Config::Config *config);
};

#endif // ITEMFACTORY_H
