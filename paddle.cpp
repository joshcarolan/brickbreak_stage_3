#include "paddle.h"
/**
 * @brief Paddle::moveLeft
 * moves thhe paddle to the left by an amount equal to the velocity of the paddle
 */
void Paddle::moveLeft()
{
    if (xPos - velocity < 0)
    {
        xPos = 0;
    }
    else
    {
        xPos-=velocity;

    }
}
/**
 * @brief Paddle::moveRight
 * moves the paddle to the right by an amount eual to the velocity of the paddle
 */
void Paddle::moveRight()
{
    if (xPos + width +velocity > windowWidth)
    {
        xPos = windowWidth - width;
    }
    else
    {
        xPos += velocity;
        //std::cout<<"xPos = "<<xPos;
    }
}
/**
 * @brief Paddle::notify
 * notifies the paddle of a keypress observed in the dialog class and calls moveLeft
 * or moveRight if the key is the specified key for that action
 * @param keypress
 * the string associated with the key that was pressed
 */
void Paddle::notify(QString keypress){
    if(keypress == QString::fromStdString(keyLeft))
    {
        moveLeft();
    }
    if(keypress == QString::fromStdString(keyRight)){
        moveRight();
    }
}
/**
 * @brief Paddle::advance
 * calls moveBy to ensure that the paddle is painted each frame
 * @param phase
 * the phase passed to the method, if it is zero the method does nothing
 */
void Paddle:: advance(int phase)
{
    if (phase == 0){
        return;
    }
    moveBy(1,1);
    moveBy(-1,-1);
}
/**
 * @brief Paddle::paint
 * renders the paddle in the scene
 * @param painter
 * the painter used to draw the paddle in the scene
 */
void Paddle:: paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setPen(QPen(Qt::black));
    painter->setBrush(QBrush(QColor(atoi(color.substr(0,3).c_str()),atoi(color.substr(3,3).c_str()),atoi(color.substr(6,3).c_str()))));
    painter->drawRect(boundingRect());
}
/**
 * @brief Paddle::boundingRect
 * the bounding rect for the paddle
 * @return
 */
QRectF Paddle::boundingRect() const
{
    return QRectF(xPos, yPos, width, height);
}


/* Converts color provided as an int to RGB string */
/**
 * @brief Paddle::colourToString
 * Converts color provided as an int to RGB string
 * @param color
 * the colour as an int
 * @return
 * the colour as an RGB string
 */
std::string Paddle::colourToString(int color)
{
    std::stringstream s, r;
    for (int i = 0; i != 3; i++, color/= 1000){
        int c = color % 1000;
        if (c < 10)
        {
            s << "00" << c;
        }
        else if (c < 100)
        {
            s << "0" << c;
        } else
        {
            s << c;
        }

    }
    r << s.str().substr(6,3) << s.str().substr(3,3) << s.str().substr(0,3);
    return r.str();

}

