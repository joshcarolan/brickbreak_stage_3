#include "ball.h"
/**
 * @brief Ball::paint
 * renders the ball in the scene based on its bounding rect which has its location and size
 * @param painter
 * the painter used to draw the ball in the scene
 */
void Ball::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *){
    QRectF rec = boundingRect();
    QBrush brush("#84D9CF");
    if(colour != "#84D9CF"){
        brush.setColor(colour); //changes the colour of the ball depending on which side it hits
    }
    painter->setBrush(brush);
    painter->drawEllipse(rec);

    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemIsMovable, true);
}

/**
 * @brief Ball::shape
 * Re-implemenation of QGraphicsItem::shape, this function returns the shape of the item as a QPainterPath.
 * The shape is used for many things including collision detection, hit tests, and for QGraphicsScene::items() functions.
 * @return
 * the shape of the item as a QPainterPath
 */
QPainterPath Ball::shape() const
{
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

/**
 * @brief Ball::brickCollision
 * Checks whether the ball collides with a brick. If ball collides with brick:
 * 1. ball bounces away
 * 2. ball notifies brick of collision
 * @param futureXPos
 * the x position the ball would be at in the next frame if it continues on its current path
 * @param futureYPos
 * the x position the ball would be at in the next frame if it continues on its current path
 * @return
 * true if there was a collision with a brick, otherwise, false
 */
bool Ball::brickCollision(int &futureXPos, int &futureYPos)
{
    int currentXPos = this->x(), currentYPos = this->y();
    bool ret = false;
    BrickComponent *brick = nullptr;

    // X Collision test
    setPos(futureXPos, currentYPos);
    auto list = collidingItems();
    if(list.size() > 0){
        ret = true;
        brick = dynamic_cast<BrickComponent *>(list.at(0));
        if (brick != 0)
        {
            if(xVelocity > 0)
            {

                futureXPos = brick->x() - radius * 2;
            } else {
                futureXPos = brick->x() + brick->boundingRect().width();
            }
            xVelocity *= -1;
        }
    }

    // Y collision test
    setPos(currentXPos, futureYPos);
    list = collidingItems();
    if(list.size() > 0){
        ret = true;
        brick = dynamic_cast<BrickComponent *>(list.at(0));
        if (brick != 0)
        {
            if(yVelocity > 0)
            {
                futureYPos = brick->y() - radius * 2;
            } else {
                futureYPos = brick->y() + brick->boundingRect().height();
            }
            yVelocity *= -1;
        }
    }

    // if hit, call brick collision method and increment the score
    if(ret){
        if(thereIsPaddle)
        {
            ++score;
            emit scoreChanged(score);
        }
        brick->collision(scene(), this);
    }
    return ret;
}
/**
 * @brief Ball::paddleCollision
 *      determines what to do in the event of a collison with the paddle
 * @param futureXPos
 *      future x position of the ball
 * @param futureYPos
 *      future y posiiton of the ball
 * @return
 *      true if there has been a collision between the ball and the paddle, otherwise false
 */

bool Ball::paddleCollision(int &futureXPos, int &futureYPos){
    int currentXPos = this->x(), currentYPos = this->y();
    bool ret = false;
    Paddle *paddle = nullptr;
    // X Collision test
    setPos(futureXPos, currentYPos);
    auto list = collidingItems();
    if(list.size() > 0){
        ret = true;
        paddle = dynamic_cast<Paddle *>(list.at(0));
        if (paddle != 0)
        {
            if(xVelocity > 0)
            {
                futureXPos = paddle->x() - radius * 2;
            } else {
                futureXPos = paddle->x() + paddle->boundingRect().width();
            }
            xVelocity *= -1;
        }
    }

    // Y collision test
    setPos(currentXPos, futureYPos);
    list = collidingItems();
    if(list.size() > 0){
        ret = true;
        paddle = dynamic_cast<Paddle *>(list.at(0));
        if (paddle != 0)
        {
            if(yVelocity > 0)
            {
                futureYPos = paddle->getYPos() - radius * 2;
            } else {
                futureYPos = paddle->getYPos() + paddle->boundingRect().height();
            }
            yVelocity *= -1;
        }
    }

    return ret;
}


/**
 * @brief Ball::advance
 *  Re-implemetation of QGraphics::advance, this function determines how to animate the next frame.
 * Below I check the window bounds with diameter rather than radius is because I set QRect boundingRect
 * to coordinates on the top left corner instead of the center.
 * This function checks for all different types of collisons that could happen in the game and adjust
 * the x-y velocitys and next position accordingly
 * @param phase
 * the phase for that the application passes to the advance function. If it is 0 do nothing.
 */
void Ball::advance(int phase)
{
    if (phase == 0)
    {
        return;
    }

    int futureXPos = this->pos().x() + xVelocity;
    int futureYPos= this->pos().y() + yVelocity;
    int diameter = radius * 2;

    if(futureYPos+2*radius>boxHeight - paddleHeight)
    {
        paddleCollision(futureXPos, futureYPos);
    }
    else
    {
        brickCollision(futureXPos, futureYPos);
    }

    if(futureYPos <= 0)
    {
        yVelocity *= -1;
        futureYPos = 0;
        colour = "#F0AFAF";
    }
    if (futureXPos <= 0)
    {
        xVelocity *= -1;
        futureXPos = 0;
        colour = "#E5C1F5";
    }
    if(thereIsPaddle)
    {
        //make the ball loose a life here and restart the game
        if(futureYPos >= boxHeight)
        {
            --lives;
            futureXPos = startX;
            futureYPos = startY;
            emit livesChanged(lives);
            if(lives ==0)
            {
                xVelocity =0;
                yVelocity =0;
                //game over
                //check to see if new high score
                checkScore();
            }

        }
    }
    else
    {
        if(futureYPos >= boxHeight - diameter)
        {
            yVelocity *= -1;
            futureYPos = boxHeight-diameter;
            colour = "#F0AFAF";
        }
    }
    if (futureXPos >= boxWidth - diameter - 1)
    {
        xVelocity*= -1;
        futureXPos = boxWidth - diameter - 1;
        colour = "#99BDF7";
    }

    setPos(futureXPos, futureYPos);

}
/**
 * @brief Ball::checkScore
 *      Checks to see if there has been a new high score posted.
 *      if there has been, overwrites the current savefile with the new high score
 */

void Ball::checkScore()
{
    std::string filename= "./../AssignmentThreeBaseOne/highScores.txt";
    std::ifstream file(filename.c_str());
    if(file.fail()){
        std::cout<<"Error: Class: Ball, Line: 224 - Missing highscore file"<<std::endl;
        exit(0);
    }
    if (file.is_open()){
        std::string highscoreString;
        int highscoreInt;
        std::getline(file, highscoreString);
        highscoreInt = atoi(highscoreString.c_str());

        if(highscoreInt<score)
        {
            std::ofstream out((filename).c_str());
            if(out.fail())
            {
                std::cout<<"Error updating high score"<<std::endl;
                exit(0);
            }
            out<<score;
            out.close();
            unlink( filename.c_str() );

        }

    }
    file.close();
}

void Ball::incrementLives(int incrementBy)
{
    lives+=incrementBy;
    emit livesChanged(lives);
}
