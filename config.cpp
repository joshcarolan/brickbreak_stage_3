#include "config.h"
/**
 * @brief Config::Config::readFile
 * reads in all of the lines of  the configuration file
 * to create all of the items for the game
 * and stores them in member variables
 */
void Config::Config::readFile(){
    std::string buffer;
    std::string buffer2;
    std::ifstream file(CONFIG_PATH.c_str());
    if(file.fail()){
        std::cout<<"Missing configuration file"<<std::endl;
        exit(0);
    }
    if (file.is_open()){
        while (std::getline(file, buffer)){
            std::stringstream line(buffer);
            std::getline(line, buffer2, ':');
            buffer2 = removeSpace(buffer2);
            if(buffer2 == "height"){
                height = atoi(getValue(buffer).c_str());
            }
            else if(buffer2 == "width"){
                width = atoi(getValue(buffer).c_str());
            }
            else if(buffer2 == "radius"){
                radius = atoi(getValue(buffer).c_str());
            }
            else if(buffer2 == "xCoordinate"){
                xCoordinate = atoi(getValue(buffer).c_str());
            }
            else if(buffer2 == "yCoordinate"){
                yCoordinate = atoi(getValue(buffer).c_str());
            }
            else if(buffer2 == "xVelocity"){
                xVelocity = atof(getValue(buffer).c_str());
            }
            else if(buffer2 == "yVelocity"){
                yVelocity = atof(getValue(buffer).c_str());
            }
            else if(buffer2 == "brick"){
                addBrick(getValue(buffer));
            }
            else if(buffer2 == "paddleHeight"){
                paddleHeight = atoi(getValue(buffer).c_str());
                std::cout<<paddleHeight<<std::endl;
            }
            else if(buffer2 == "paddleWidth"){
                paddleWidth = atoi(getValue(buffer).c_str());
            }
            else if(buffer2 == "colour"){
                paddleColour = atoi(getValue(buffer).c_str());

            }
            else if(buffer2 == "startXPosition"){
                paddleStartX = atoi(getValue(buffer).c_str());
            }
            else if(buffer2 == "velocity"){
                paddleVelocity = atoi(getValue(buffer).c_str());
            }
            else if(buffer2 == "ballLives"){
                ballLives = atoi(getValue(buffer).c_str());
            }
            else if(buffer2 == "movePaddleLeft"){
                leftCommand = getValue(buffer).at(0);

            }
            else if(buffer2 == "movePaddleRight"){
                rightCommand =getValue(buffer).at(0);
            }
        }
        file.close();
    }
    validate();
}
/**
 * @brief Config::Config::removeSpace
 *      simply removes the spaces from a string
 * @param s
 *      the string to remove the spaces from
 * @return
 *      the string s without any spaces
 */
std::string Config::Config::removeSpace(std::string s){
    std::string newStr;
    for(std::string::size_type i = 0; i<s.length(); i++){
        if(s.at(i) != ' '){
            newStr += s.at(i);
        }
    }
    return newStr;
}

std::string Config::Config::getValue(std::string str) const
{
    return str.substr(str.find(":") + 2, str.length());
}
/**
 * @brief Config::Config::addBrick
 *      creates a map of string double pairs of configuration items for a brick
 * @param str
 *      the string containing all of the configuration items for one brick
 */
void Config::Config::addBrick(std::string str)
{
    std::map<std::string, double> brick;

    while (str.find(":") != std::string::npos)
    {
        int i = str.find(":");
        std::string key = str.substr(0, i);
        str = str.substr(i + 1, str.length());

        int j = str.find(" ");
        std::string value = str.substr(0, j);
        str = str.substr(j + 1, str.length());
        brick.insert(std::pair<std::string, double>(key, atof(value.c_str())));
    }
    bricks.push_back(brick);
}
/**
 * @brief Config::Config::validate
 *      checks to ensure the entered configurations are sensible
 */
void Config::Config::validate(){
    if(width < 0  ||
            height < 0 ||
            radius < 0 ||
            xCoordinate < 0 ||
            yCoordinate < 0 ||
            ballLives < 0 ||
            paddleWidth < 0 ||
            paddleHeight < 0 ||
            paddleStartX < 0 ){
        std::cout<<"Cannot have negative width, height, radius, ballLives, paddle starting x position, paddle width, paddle height or xy coordinates"<<std::endl;
        exit(0);
    }

    if(height < yCoordinate + 2*radius ||
            width < xCoordinate + 2*radius){
        std::cout<<"The ball doesn't fit within the box dimensons"<<std::endl;
        exit(0);
    }
    if (paddleWidth> width || paddleHeight >  height){
        std::cout<<"The paddle doesn't fit within the box dimensons"<<std::endl;
        exit(0);
    }
    if (paddleStartX + paddleWidth> width ){
        std::cout<<"The paddles starting position doesn't fit within the box dimensons"<<std::endl;
        exit(0);
    }

    for (auto brick = bricks.begin(); brick != bricks.end(); brick++)
    {
        if (brick->size() != 8 && brick->size() != 11)
        {
            std::cout<<"Incorrect number of brick parameters provided"<<std::endl;
            exit(0);
        }

    }
}
